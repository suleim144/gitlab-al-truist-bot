FROM docker.io/bash:5.1.16-alpine3.15
WORKDIR /bin
COPY ./gitlab-al-truist-bot /bin/gitlab-al-truist-bot
COPY ./models /models
RUN chmod +x /bin/gitlab-al-truist-bot
CMD ["sh", "-c", "gitlab-al-truist-bot"]
