{{/*
Expand the name of the chart.
*/}}
{{- define "gitlab-al-truist-bot.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "gitlab-al-truist-bot.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "gitlab-al-truist-bot.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "gitlab-al-truist-bot.labels" -}}
helm.sh/chart: {{ include "gitlab-al-truist-bot.chart" . }}
{{ include "gitlab-al-truist-bot.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "gitlab-al-truist-bot.selectorLabels" -}}
app.kubernetes.io/name: {{ include "gitlab-al-truist-bot.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "gitlab-al-truist-bot.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "gitlab-al-truist-bot.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
{{/*
env variables
*/}}
{{- define "generateNormalENVConfig" -}}
{{- range $key, $value := .Values.env.normal }}
- name: {{ $key }}
  value: {{$value | quote}}
{{- end -}}
{{- end -}}

{{- define "generateSecretENVConfig"}}
{{- $loadableEnvsecretName  := .Values.loadableEnvsecret.name }}
{{- if .Values.env.secret }}
{{- range $key, $val := .Values.env.secret }}
- name: {{ $key }}
  valueFrom:
    secretKeyRef:
      name: {{ $loadableEnvsecretName }}
      key: {{ $key | quote}}
{{- end }}
{{- else }}
{{- range $key, $val := .Values.env.sealedSecret }}
- name: {{ $key }}
  valueFrom:
    secretKeyRef:
      name: {{ $loadableEnvsecretName }}
      key: {{ $key | quote}}
{{- end }}
{{- end }}
{{- end }}

{{- define "generateSealedSecret"}}
{{- range $key, $value := .Values.env.sealedSecret }}
{{ $key }}: {{ $value }}
{{- end -}}
{{- end -}}