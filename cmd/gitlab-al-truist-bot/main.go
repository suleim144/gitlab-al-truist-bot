package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/classifier"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/keyword"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/server"
)

var (
	log logger.Logger
	svr *http.Server
)

func main() {

	// listen for exit codes
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)
	go listenForShutdownSig(sig)

	// load config
	cfg, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}
	// Create logger
	log, err := logger.NewZapLogger(true)
	if err != nil {
		panic(err)
	}
	// Create Question detector
	QuestionDetector, err := classifier.LoadNLPQuestionProcessor(cfg, log)
	if err != nil {
		panic(err)
	}
	defer QuestionDetector.Shutdown()

	// Create Key word extractor
	KeyWordExtractor, err := keyword.NewKeyWordExtarction(cfg)
	if err != nil {
		panic(err)
	}
	defer KeyWordExtractor.Shutdown()

	// Create server
	svr, err = server.NewServer(cfg, log, QuestionDetector, KeyWordExtractor)
	if err != nil {
		panic(err)
	}
	defer svr.Shutdown(context.Background())

	// listen and server
	log.Info("starting service on http://0.0.0.0:%s/", cfg.Port)
	if err := svr.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		panic(err)
	}

}

func listenForShutdownSig(sig <-chan os.Signal) {
	<-sig
	shutdown()
}

func shutdown() {
	if log != nil {
		log.Shutdown()
	}
	if svr != nil {
		svr.Shutdown(context.Background())
	}
}
