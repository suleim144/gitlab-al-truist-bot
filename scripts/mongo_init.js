db.createUser({
  user: 'root',
  pwd: 'test1234',
  roles: [
    {
      role: 'dbOwner',
      db: 'backend',
    },
  ],
});

db.createCollection("team");
db.createCollection("channelConfig");
db.team.insert(
  [
    {
      "id": "T9TK3CUKW",
      "config": {
        "access_token": "xoxe.xoxb-1-..",
        "token_type": "bot",
        "scope": "commands,incoming-webhook",
        "bot_user_id": "U0KRQLJ9H",
        "app_id": "A0KRD7HC3",
        "expires_in": 43200,
        "refresh_token": "xoxe-1-...",
        "team": {
            "name": "Slack Softball Team",
            "id": "T9TK3CUKW"
        },
        "enterprise": {
            "name": "slack-sports",
            "id": "E12345678"
        },
        "authed_user": {
            "id": "U1234",
            "scope": "chat:write",
            "access_token": "xoxe.xoxp-1234",
            "expires_in": 43200,
            "refresh_token": "xoxe-1-...",
            "token_type": "user"
        },
        "incoming_webhook": {
          "url": "https://hooks.slack.com/TXXXXX/BXXXXX/XXXXXXXXXX",
          "channel": "#channel-it-will-post-to",
          "configuration_url": "https://teamname.slack.com/services/BXXXXX"
        }
    }
}
    
  ]
)

db.channelConfig.insert(
  [
    {
      "teamid": "XXXXXXXXXX",
      "channelid": "xoxp-XXXXXXXX-XXXXXXXX-XXXXX",
      "config":{
        "channel_id": "xoxp-XXXXXXXX-XXXXXXXX-XXXXX",
        "channel_name": "name",
        "user_id": "name",
        "user_name": "name",
        "team_id": "XXXXXXXXXX",
        "team_domain": "XXXXXXXXXX",
        "gitlab": {
            "project_id": "https://hooks.slack.com/TXXXXX/BXXXXX/XXXXXXXXXX",
            "access_token": "xoxp-XXXXXXXX-XXXXXXXX-XXXXX",
        }
      }
  }
  ]
)