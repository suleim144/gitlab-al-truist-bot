#!/usr/bin/env bash

set -ex
echo "" > ${CI_BUILDS_DIR}/${CI_PROJECT_PATH}/.coverage.out

for d in $(go list ./... | grep -v -e .cache -e mocks); do
    go test -race -coverprofile=profile.out -covermode=atomic $d
    if [ -f profile.out ]; then
        cat profile.out >> ${CI_BUILDS_DIR}/${CI_PROJECT_PATH}/.coverage.out
        rm profile.out
    fi
done
