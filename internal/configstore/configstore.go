package configstore

import (
	"context"

	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
)

type ConfigDB interface {
	GetChannelConfig(ctx context.Context, teamId, channelId string) (*models.Channel, error)
	GetTeamConfig(ctx context.Context, teamId string) (*models.Team, error)
	UpsertTeamConfig(ctx context.Context, teamId string, config *models.OAuthResponse) error
	UpsertChannelConfig(ctx context.Context, teamId string, channelId string, config *models.ChannelConfig) error
}

type TeamConfig struct {
	TeamID string               `bson:"id"`
	Config models.OAuthResponse `bson:"config"`
}
type ChannelConfig struct {
	ChanelID      string `bson:"id"`
	GitlabToken   string `bson:"gtoken"`
	GitlabProject string `bson:"gproject"`
}

type ConfigStore interface {
	GetChannelConfig(ctx context.Context, teamId, channelId string) (*models.ChannelConfig, error)
	GetTeamConfig(ctx context.Context, teamId string) (*models.OAuthResponse, error)
	UpsertTeamConfig(ctx context.Context, teamId string, config *models.OAuthResponse) error
	UpsertChannelConfig(ctx context.Context, teamId string, channelId string, config *models.ChannelConfig) error
}
type DefaultConfigStore struct {
	db ConfigDB
}

func NewConfigStore(db ConfigDB) (*DefaultConfigStore, error) {
	return &DefaultConfigStore{db}, nil
}

func (cs *DefaultConfigStore) GetChannelConfig(ctx context.Context, teamId, channelId string) (*models.ChannelConfig, error) {
	cc, err := cs.db.GetChannelConfig(ctx, teamId, channelId)
	if err != nil {
		return nil, err
	}
	if cc == nil {
		return nil, nil
	}
	return cc.Config, nil

}
func (cs *DefaultConfigStore) GetTeamConfig(ctx context.Context, teamId string) (*models.OAuthResponse, error) {
	tc, err := cs.db.GetTeamConfig(ctx, teamId)
	if err != nil {
		return nil, err
	}
	if tc == nil {
		return nil, nil
	}
	return tc.Config, nil
}

func (cs *DefaultConfigStore) UpsertTeamConfig(ctx context.Context, teamId string, config *models.OAuthResponse) error {
	return cs.db.UpsertTeamConfig(ctx, teamId, config)
}

func (cs *DefaultConfigStore) UpsertChannelConfig(ctx context.Context, teamId string, channelId string, config *models.ChannelConfig) error {
	return cs.db.UpsertChannelConfig(ctx, teamId, channelId, config)
}
