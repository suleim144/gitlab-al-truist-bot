package configstore

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	mocks "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/configstore"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
)

func TestGetChannelConfig_Error(t *testing.T) {
	mockCfgDB := mocks.NewConfigDB(t)
	mockCfgDB.On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(
		nil, assert.AnError)
	configStore, _ := NewConfigStore(mockCfgDB)
	channelConfig, err := configStore.GetChannelConfig(context.Background(), mock.Anything, mock.Anything)
	assert.Empty(t, channelConfig)
	assert.Error(t, err)
}
func TestGetTeamConfig_Error(t *testing.T) {
	mockCfgDB := mocks.NewConfigDB(t)
	mockCfgDB.On("GetTeamConfig", mock.Anything, mock.Anything).Return(
		nil, assert.AnError)
	configStore, _ := NewConfigStore(mockCfgDB)
	teamConfig, err := configStore.GetTeamConfig(context.Background(), mock.Anything)
	assert.Empty(t, teamConfig)
	assert.Error(t, err)
}

func TestGetChannelConfig_Empty(t *testing.T) {
	mockCfgDB := mocks.NewConfigDB(t)
	mockCfgDB.On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(
		&models.Channel{}, nil)
	configStore, _ := NewConfigStore(mockCfgDB)
	channelConfig, err := configStore.GetChannelConfig(context.Background(), mock.Anything, mock.Anything)
	assert.Empty(t, channelConfig)
	assert.Empty(t, err)
}
func TestGetTeamConfig_Empty(t *testing.T) {
	mockCfgDB := mocks.NewConfigDB(t)
	mockCfgDB.On("GetTeamConfig", mock.Anything, mock.Anything).Return(
		&models.Team{}, nil)
	configStore, _ := NewConfigStore(mockCfgDB)
	teamConfig, err := configStore.GetTeamConfig(context.Background(), mock.Anything)
	assert.Empty(t, teamConfig)
	assert.Empty(t, err)
}

func TestGetChannelConfig(t *testing.T) {
	mockCfgDB := mocks.NewConfigDB(t)
	mockCfgDB.On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(
		&models.Channel{Config: &models.ChannelConfig{}}, nil)
	configStore, _ := NewConfigStore(mockCfgDB)
	channelConfig, err := configStore.GetChannelConfig(context.Background(), mock.Anything, mock.Anything)
	assert.NotNil(t, channelConfig)
	assert.Empty(t, err)
}
func TestGetTeamConfig(t *testing.T) {
	mockCfgDB := mocks.NewConfigDB(t)
	mockCfgDB.On("GetTeamConfig", mock.Anything, mock.Anything).Return(
		&models.Team{Config: &models.OAuthResponse{}}, nil)
	configStore, _ := NewConfigStore(mockCfgDB)
	teamConfig, err := configStore.GetTeamConfig(context.Background(), mock.Anything)
	assert.NotNil(t, teamConfig)
	assert.Empty(t, err)
}
