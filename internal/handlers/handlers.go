package handlers

import (
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/classifier"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/configstore"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/gitlab"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/keyword"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

type RouteDependencies struct {
	Log                  logger.Logger
	Cfg                  *config.Config
	ConfigStore          configstore.ConfigStore
	Qd                   classifier.QuestionDetector
	KeyEx                keyword.KeyWordExtractor
	SlackComposerFactory slack.SlackFactory
	GitlabClientFactory  gitlab.GitLabClientFactory
}
