package v1

import (
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/classifier"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/configstore"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/gitlab"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/keyword"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

type AlwaysTrueQuestionClassifier struct {
}

type AlwaysFalseQuestionClassifier struct {
}

func (atqd *AlwaysTrueQuestionClassifier) Classify(text string) (bool, error) {
	return true, nil
}

func (atqd *AlwaysFalseQuestionClassifier) Classify(text string) (bool, error) {
	return false, nil
}

// Helper function to get a router with logging capabilities
func getRouter(t *testing.T) *gin.Engine {
	r := gin.Default()
	r.Use(func(c *gin.Context) {
		requestByte, err := httputil.DumpRequest(c.Request, true)
		if err != nil {
			t.Logf("error dumping request: %s", err)
		}
		t.Logf("request: %s \n", string(requestByte[:]))
	}) // new line
	return r
}

// Helper function to process a request and test its response
func testHTTPResponse(t *testing.T, r *gin.Engine, req *http.Request, f func(t *testing.T, w *httptest.ResponseRecorder)) {

	// Create a response recorder
	w := httptest.NewRecorder()

	// Create the service and process the above request.
	r.ServeHTTP(w, req)

	f(t, w)
}

type opt func(*handlers.RouteDependencies) *handlers.RouteDependencies

func WithStdLogger(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
	dep.Log = logger.NewStdLogger(true)
	return dep
}

func WithQuestionDecider(qd classifier.QuestionDetector) opt {
	return func(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
		dep.Qd = qd
		return dep
	}
}

func WithConfig(cfg *config.Config) opt {
	return func(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
		dep.Cfg = cfg
		return dep
	}
}

func WithKeyWordExtractor(keyEx keyword.KeyWordExtractor) opt {
	return func(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
		dep.KeyEx = keyEx
		return dep
	}
}

func WithSlackComposerFactory(slackComposerFactory slack.SlackFactory) opt {
	return func(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
		dep.SlackComposerFactory = slackComposerFactory
		return dep
	}
}

func WithGitlabClientFactory(gitlabClientFactory gitlab.GitLabClientFactory) opt {
	return func(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
		dep.GitlabClientFactory = gitlabClientFactory
		return dep
	}
}

func WithConfigStore(configstore configstore.ConfigStore) opt {
	return func(dep *handlers.RouteDependencies) *handlers.RouteDependencies {
		dep.ConfigStore = configstore
		return dep
	}
}
func setupSuite(t *testing.T, opts ...opt) (func(t *testing.T), *handlers.RouteDependencies) {
	routeDep := &handlers.RouteDependencies{}
	for _, opt := range opts {
		routeDep = opt(routeDep)
	}
	return func(t *testing.T) {}, routeDep
}

func setupHandlerTest(t *testing.T, routeDep *handlers.RouteDependencies,
	handler func(handlers.RouteDependencies) gin.HandlerFunc,
	opts ...opt) (func(t *testing.T), *gin.Engine) {
	for _, opt := range opts {
		routeDep = opt(routeDep)
	}
	r := getRouter(t)
	r.Use(handler(*routeDep))
	return func(t *testing.T) {}, r
}

func setupTest(t *testing.T, routeDep *handlers.RouteDependencies, opts ...opt) func(t *testing.T) {
	for _, opt := range opts {
		routeDep = opt(routeDep)
	}
	return func(t *testing.T) {}
}
