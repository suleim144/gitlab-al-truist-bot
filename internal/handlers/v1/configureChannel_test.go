package v1

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	cs_mocks "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/configstore"
	slack_mock "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/slack"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

// Event handler
func TestConfigureChannel_CorrectCommand(t *testing.T) {
	// Basic Setup: Adding logger
	suiteTearDown, routeDep := setupSuite(t, WithStdLogger)
	defer suiteTearDown(t)

	// Advance Setup: Configuring mocks
	slackComposerFactory := slack_mock.NewSlackFactory(t)
	slackMock := slack_mock.NewSlack(t)
	slackMock.On("VerifySignature", mock.Anything, mock.Anything).
		Return(true, nil)
	slackMock.On("ParseCmd", mock.Anything).
		Return(&slack.ParsedCmd{
			Cmd: "/gal",
			Args: map[string]string{
				"searchProject": "deviceIssues",
				"useToken":      "xv234ce",
			},
			EventMeta: &models.CmdEventMeta{},
		}, nil)
	slackComposerFactory.On("SlackCompose", mock.Anything).Return(slackMock)
	configstoreMock := cs_mocks.NewConfigStore(t)
	configstoreMock.On("UpsertChannelConfig", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	testTearDown, router := setupHandlerTest(t, routeDep, ConfigureChannel,
		WithConfig(&config.Config{}),
		WithQuestionDecider(&AlwaysFalseQuestionClassifier{}),
		WithSlackComposerFactory(slackComposerFactory),
		WithConfigStore(configstoreMock),
	)
	defer testTearDown(t)

	// Setup test request
	var buf bytes.Buffer
	_, err := buf.Write([]byte(
		`token=pJhDiPvEcbK5Lq4HbQI2hvC8&team_id=T02B8CZ64LB&team_domain=yaengineers&channel_id=C04NRH29P7S&channel_name=test&user_id=U02BEPQUWKE&user_name=suleimi.ahmed&command=%2Fgal&text=searchProject%3DgitlabProject+useToken%3DgC04NRH29P7S&api_app_id=A04NPT8EDQ8&is_enterprise_install=false&response_url=https%3A%2F%2Fhooks.slack.com%2Fcommands%2FT02B8CZ64LB%2F4756010048068%2FP7dYKwx7YUf28c2ItTyBPnQV&trigger_id=4766180010033.2382441208691.ce551da35b43c692d8753fd01f6a0e92`),
	)
	assert.NoError(t, err)
	req, err := http.NewRequest("POST", "/", &buf)
	assert.NoError(t, err)
	expectedResponseBody := map[string]string{"response_type": "in_channel", "text": "al has been configured for this channel!"}

	// Make test request
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	var actualResponse map[string]string
	err = json.Unmarshal(w.Body.Bytes(), &actualResponse)
	assert.NoError(t, err)
	// assert response
	assert.Equal(t, http.StatusOK, w.Code, "HTTP code does not match expected")
	assert.Equal(t, expectedResponseBody, actualResponse, "response body does not match expected")
}
