package v1

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/apierrors"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/gitlab"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

// Event handler
func EventHandler(routeDep handlers.RouteDependencies) gin.HandlerFunc {
	scomposer :=
		routeDep.SlackComposerFactory.SlackCompose(&slack.SlackCredentials{
			SlackSigningSecret: routeDep.Cfg.SlackSigningfSecret,
		})
	log := routeDep.Log
	return func(c *gin.Context) {
		// extract the body
		data, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			log.Infof("failed to read the request body: %v", err)
		}
		// Replace the body with a new reader after reading from the original
		c.Request.Body = io.NopCloser(bytes.NewBuffer(data))
		// Verify the request came from a credible party
		verified, err := scomposer.VerifySignature(c.Request.Header, data)
		if err != nil {
			err = fmt.Errorf("failed verification wiith error: %v", err)
			log.Infof(err.Error())
		}
		if !verified {
			err = fmt.Errorf("failed verification")
			log.Info(err.Error())
		}
		// parse the request
		parsedEvent, err := scomposer.ParseEvent(data)
		if err != nil {
			log.Infof("could not parse request: %v", err)
			apierrors.JSONHandleError(c.Writer, apierrors.WrapError(err, apierrors.ErrBadRequest))
			c.Abort()
			return
		}
		// check if verification request
		if parsedEvent.IsURLVerificationRequest() {
			challange, err := parsedEvent.ExtractChallange()
			if err != nil {
				log.Infof("could not parse request: %v", err)
				apierrors.JSONHandleError(c.Writer, apierrors.WrapError(err, apierrors.ErrBadRequest))
				c.Abort()
				return
			}
			c.JSON(http.StatusOK, struct{ challange string }{challange: challange})
			return
		}
		// respond back fast to slack event engine after verification, to avoid trothling.
		// send any proceeding responses to the message asynchronously if needed.
		go handleEventAsyncResponse(routeDep, parsedEvent)
		c.JSON(http.StatusOK, gin.H{"status": "RECEIVED"})

	}
}

func handleEventAsyncResponse(routeDep handlers.RouteDependencies, eventsAPIEvent *slack.ParsedEvent) error {

	log := routeDep.Log
	keyEx := routeDep.KeyEx
	configStore := routeDep.ConfigStore
	qd := routeDep.Qd
	ctx, cncl := context.WithTimeout(context.Background(), 10*time.Second)
	defer cncl()
	// Send call back to slack cahnnel if necessary
	if eventsAPIEvent.IsACallBackEvent() {
		if eventsAPIEvent.InnerEvent.IsAMessageEvent() {
			// we received a message in a channel
			// extract the event
			msgEvent := eventsAPIEvent.InnerEvent.GetMessageEvent()
			// discard any messages in any channel that came from a bot (to prevent echoing)
			if msgEvent.BotID != "" {
				log.Infof("skipping processing a bot message: %v", msgEvent.Text)
				return nil
			}

			// get the channel configurations
			cconfig, err := configStore.GetChannelConfig(ctx, eventsAPIEvent.TeamID, msgEvent.Channel)
			if err != nil {
				err := fmt.Errorf("error fetching channel config for team:%v and channel:%v : %w", eventsAPIEvent.TeamID, msgEvent.Channel, err)
				log.Info(err)
				return err
			}
			if cconfig == nil {
				err := fmt.Errorf("channel config for team:%v and channel:%v not found", eventsAPIEvent.TeamID, msgEvent.Channel)
				log.Info(err)
				// we only care about channels configured for us and that have message events
				return err
			}
			// check if the message received is a question or not
			isQuestion, err := qd.Classify(msgEvent.Text)
			if err != nil {
				log.Infof("error classifying statement: %v: %w", msgEvent.Text, err)
				return err
			}
			if !isQuestion {
				log.Infof("request:\"%v\" from team:%v and channel:%v was not a question", msgEvent.Text, eventsAPIEvent.TeamID, msgEvent.Channel)
				// we only care about questons
				return nil
			}
			// Get the slack team config
			teamConfig, err := configStore.GetTeamConfig(ctx, eventsAPIEvent.TeamID)
			if err != nil {
				log.Infof("error fetching team config for team:%s: %w", eventsAPIEvent.TeamID, err)
				return err
			}
			if teamConfig == nil {
				err = fmt.Errorf("team config for team:%s not found", eventsAPIEvent.TeamID)
				log.Info(err)
				// we only care about channels configured for us and that have message events
				return err
			}
			// extract the key words from the question that was asked
			keywords, err := keyEx.Extract(msgEvent.Text, "")
			if err != nil {
				log.Infof("error extracting key word from question: \"%s\": %w", eventsAPIEvent.TeamID, err)
				return err
			}
			// if the key word is less than or equal to one we dont want to proceed with searching for
			// gitlab issues as the results might be too narrow
			if len(keywords) <= 1 {
				err := fmt.Errorf("not enough keywords to conduct a search: %v", keywords)
				log.Info(err)
				return err
			}
			// search the gitlab project configured for the channel for any matching issues
			searchWords := strings.Join(keywords, " ")

			if cconfig.GitlabConfig == nil {
				err := fmt.Errorf("gitlab config not found for team id: %s in channel: %s", eventsAPIEvent.TeamID, eventsAPIEvent.InnerEvent.GetMessageEvent().Channel)
				log.Info(err)
				return err
			}
			// create a gitlab client with the gitlab token configured for the channel that made the request
			gitlabClient, err := routeDep.GitlabClientFactory.NewClient(cconfig.GitlabConfig.AccessToken)
			if err != nil {
				err := fmt.Errorf("failed to connect to gitlab: %w", err)
				return err
			}
			issues, err := gitlabClient.GetIssuesWithKeyWords(cconfig.GitlabConfig.ProjectID, searchWords)
			if err != nil {
				err := fmt.Errorf("failed to get issues from gitlab: %w", err)
				log.Info(err)
				return err
			}
			if len(issues) == 0 {
				err := fmt.Errorf("issues not found for keyword: %v", keywords)
				log.Info(err)
				return err
			}

			slackAttch := convertGitLabIssuesToSlackAttatchment(issues)

			// post issues to slack
			scomposer :=
				routeDep.SlackComposerFactory.SlackCompose(&slack.SlackCredentials{
					SlackSigningSecret: routeDep.Cfg.SlackSigningfSecret,
					SlackClientID:      routeDep.Cfg.SlackClientID,
					SlackClientSecret:  routeDep.Cfg.SlackClientSecret,
				})
			err = scomposer.PostAttachmentWithNewClient(teamConfig.AccessToken, msgEvent.Channel, slackAttch,
				"Hey! Thanks for the question! Would any of the following help answer your question :eyes: :")
			if err != nil {
				err := fmt.Errorf("failed to post question answer to slack: %w", err)
				log.Info(err)
				return err
			}
		}
	}
	return nil
}

func convertGitLabIssuesToSlackAttatchment(issues []gitlab.Issue) []slack.Attachment {
	var slackAtt []slack.Attachment
	for _, issue := range issues {
		slackAtt = append(slackAtt, slack.Attachment{
			Title:     issue.Title,
			TitleLink: issue.URL,
		})
	}
	return slackAtt
}
