package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
)

// Dummy handler
func Dummy(routeDep handlers.RouteDependencies) gin.HandlerFunc {
	return func(c *gin.Context) {}
}
