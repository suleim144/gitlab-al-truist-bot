package v1

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/apierrors"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

// Event handler
func SlackTokenExchangeHandler(routeDep handlers.RouteDependencies) gin.HandlerFunc {
	slackComposer :=
		routeDep.SlackComposerFactory.SlackCompose(&slack.SlackCredentials{
			SlackSigningSecret: routeDep.Cfg.SlackSigningfSecret,
			SlackClientID:      routeDep.Cfg.SlackClientID,
			SlackClientSecret:  routeDep.Cfg.SlackClientSecret,
		})
	log := routeDep.Log
	cfgStore := routeDep.ConfigStore
	return func(c *gin.Context) {
		// parse params
		var code, redirectURI string
		var exist bool
		if code, exist = c.GetQuery("code"); !exist {
			err := apierrors.WrapError(fmt.Errorf("%s", "code param is missing"), apierrors.ErrBadRequest)
			apierrors.JSONHandleError(c.Writer, err)
			c.Abort()
			return
		}
		if redirectURI, exist = c.GetQuery("redirect_uri"); !exist {
			err := apierrors.WrapError(fmt.Errorf("%s", "redirect_uri param is missing"), apierrors.ErrBadRequest)
			apierrors.JSONHandleError(c.Writer, err)
			c.Abort()
			return
		}
		// exchange token
		OAuthResponse, err := slackComposer.GetOAuth2Token(c.Request.Context(), code, redirectURI)
		if err != nil || OAuthResponse == nil {
			log.Infof("failed to exchange code for token: %v", err)
			err := apierrors.WrapError(fmt.Errorf("failed to exchange code for token: %s", err), apierrors.ErrWrongAccess)
			apierrors.JSONHandleError(c.Writer, err)
			c.Abort()
			return
		}
		err = cfgStore.UpsertTeamConfig(c.Request.Context(), OAuthResponse.Team.ID, OAuthResponse)
		if err != nil {
			log.Infof("failed to upsert team config for team: %s :", OAuthResponse.Team.ID, err)
			err := apierrors.WrapError(err, apierrors.ErrInternalError)
			apierrors.JSONHandleError(c.Writer, err)
			c.Abort()
			return
		}
		c.JSON(200, gin.H{"status": "SUCCESS"})
	}
}
