package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/apierrors"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers/driver"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

// Event handler
func ConfigureChannel(routeDep handlers.RouteDependencies) gin.HandlerFunc {
	slackComposer :=
		routeDep.SlackComposerFactory.SlackCompose(&slack.SlackCredentials{
			SlackSigningSecret: routeDep.Cfg.SlackSigningfSecret,
			SlackClientID:      routeDep.Cfg.SlackClientID,
			SlackClientSecret:  routeDep.Cfg.SlackClientSecret,
		})
	log := routeDep.Log
	channelStorage := routeDep.ConfigStore

	return func(c *gin.Context) {
		// store the content of the body in a slice for usge by all functions
		data, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			log.Infof("failed to read the request body: %w", err)
			c.JSON(200, gin.H{"response_type": "in_channel", "text": "failed with the error: " + err.Error()})
			//apierrors.JSONHandleError(c.Writer, wrappedError)
			c.Abort()
			return
		}
		// Replace the body with a new reader after reading from the original
		c.Request.Body = io.NopCloser(bytes.NewBuffer(data))

		// Verify the request came from a credible party
		verified, err := slackComposer.VerifySignature(c.Request.Header, data)
		if err != nil {
			err = fmt.Errorf("failed verification wiith error: %w", err)
			log.Infof(err.Error())
			c.JSON(200, gin.H{"response_type": "in_channel", "text": err.Error()})
			c.Abort()
			return
		}
		if !verified {
			err = fmt.Errorf("failed verification")
			log.Info(err.Error())
			wrappedError := apierrors.WrapError(err, apierrors.ErrWrongAccess)
			c.JSON(200, gin.H{"response_type": "in_channel", "text": wrappedError.Error()})
			c.Abort()
			return
		}

		// parse Cmd params
		parsedCmd, err := slackComposer.ParseCmd(c.Request)
		if err != nil {
			err = fmt.Errorf("could not parse the slack command: %v", err)
			log.Info(err.Error())
			wrappedError := apierrors.WrapError(err, apierrors.ErrBadRequest)
			c.JSON(200, gin.H{"response_type": "in_channel", "text": wrappedError.Error()})
			c.Abort()
			return
		}

		// send the cmd to the command router
		err = routeCmd(c.Request.Context(), parsedCmd, channelStorage)
		if err != nil {
			err = fmt.Errorf("could not process the slack command: %v", err)
			log.Info(err.Error())
			wrappedError := apierrors.WrapError(err, apierrors.ErrBadRequest)
			c.JSON(200, gin.H{"response_type": "in_channel", "text": wrappedError.Error()})
			c.Abort()
			return
		}

		// send success
		c.JSON(200, gin.H{"response_type": "in_channel", "text": "al has been configured for this channel!"})

		// TODO: send a request back to the channel comfirming configuration
	}
}

func routeCmd(ctx context.Context, pcmd *slack.ParsedCmd, channelStore driver.ChannelStorage) error {

	switch pcmd.Cmd {
	case "/gal":
		return handleGalCmd(ctx, pcmd, channelStore)
	default:
		return fmt.Errorf("invalid command: %s", pcmd.Cmd)

	}
}

func handleGalCmd(ctx context.Context, pcmd *slack.ParsedCmd, channelStore driver.ChannelStorage) error {

	// Convert map to json string
	jsonStr, err := json.Marshal(pcmd.Args)
	if err != nil {
		return err
	}

	// Convert json string to struct
	var galArgs models.GalArgs
	err = json.Unmarshal(jsonStr, &galArgs)
	if err != nil {
		return err
	}
	err = validateGalArgs(&galArgs)
	if err != nil {
		return err
	}

	channelConfig := unpackChannelConfig(pcmd, &galArgs)

	err = channelStore.UpsertChannelConfig(ctx, channelConfig.TeamID,
		channelConfig.ChannelID, channelConfig)
	if err != nil {
		//log.Infof("failed to upsert channel config for channel: %s of team: %s :",
		//channelConfig.ChannelID, channelConfig.TeamID, err)
		err := apierrors.WrapError(err, apierrors.ErrInternalError)
		return err
	}

	return nil
}

func validateGalArgs(galArgs *models.GalArgs) error {
	if galArgs.ProjectID == "" {
		//log.Info("command is missing searchProject=")
		return errors.New("missing searchProject")
	}
	if galArgs.AccessToken == "" {
		//log.Info("command is missing useToken=")
		return errors.New("missing useToken")
	}
	return nil
}

func unpackChannelConfig(pcmd *slack.ParsedCmd, galArgs *models.GalArgs) *models.ChannelConfig {

	cconfig := &models.ChannelConfig{
		ChannelID:   pcmd.EventMeta.ChannelID,
		ChannelName: pcmd.EventMeta.ChannelName,
		TeamID:      pcmd.EventMeta.TeamID,
		TeamDomain:  pcmd.EventMeta.TeamDomain,
		UserID:      pcmd.EventMeta.UserID,
		UserName:    pcmd.EventMeta.UserName,
		GitlabConfig: &models.GitlabConfig{
			ProjectID:   galArgs.ProjectID,
			AccessToken: galArgs.AccessToken,
		},
	}
	// unpack
	return cconfig

}
