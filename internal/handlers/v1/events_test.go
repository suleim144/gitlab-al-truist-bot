package v1

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/gitlab"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
	qd_mocks "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/classifier"
	cs_mocks "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/configstore"
	gitlab_mocks "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/gitlab"
	keyword_mock "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/keyword"
	slack_mock "gitlab.com/suleim144/gitlab-al-truist-bot/internal/mocks/slack"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

var AMessageEvent = `{
	"token":"pJhDiPvEcbK5Lq4HbQI2hvC8",
	"team_id":"T02B8CZ64LB",
	"context_team_id":"T02B8CZ64LB",
	"context_enterprise_id":null,
	"api_app_id":"A04NPT8EDQ8",
	"event":{
		"client_msg_id":"af1e950e-9ae9-426d-b812-c02e8f2ba6d8",
		"type":"message",
		"text":"hey this is a question?",
		"user":"U02BEPQUWKE",
		"ts":"1675707637.048429",
		"blocks":[
			{
				"type":"rich_text",
				"block_id":"ja=",
				"elements":[
					{
						"type":"rich_text_section",
						"elements":[
							{
								"type":"text",
								"text":"hey this is a question?"
							}
						]
					}
				]
			}
		],
		"team":"T02B8CZ64LB",
		"channel":"C04N9MA53FD",
		"event_ts":"1675707637.048429",
		"channel_type":"channel"
	},
	"type":"event_callback",
	"event_id":"Ev04N6TBFJNR",
	"event_time":1675707637,
	"authorizations":[
		{
			"enterprise_id":null,
			"team_id":"T02B8CZ64LB",
			"user_id":"U04NQ5B7W56",
			"is_bot":true,
			"is_enterprise_install":false
		}
	],
	"is_ext_shared_channel":false,
	"event_context":"4-eyJldCI6Im1lc3NhZ2UiLCJ0aWQiOiJUMDJCOENaNjRMQiIsImFpZCI6IkEwNE5QVDhFRFE4IiwiY2lkIjoiQzA0TjlNQTUzRkQifQ"
}`

func TestMessageEvent_Received(t *testing.T) {
	// Basic Setup: Adding logger
	suiteTearDown, routeDep := setupSuite(t, WithStdLogger)
	defer suiteTearDown(t)

	// Advance Setup: Configuring mocks
	slackComposerFactory := slack_mock.NewSlackFactory(t)
	slackMock := slack_mock.NewSlack(t)
	slackMock.On("VerifySignature", mock.Anything, mock.Anything).
		Return(true, nil)
	slackMock.On("ParseEvent", mock.Anything).
		Return(&slack.ParsedEvent{
			Type: slack.CallbackEventKey,
		}, nil)
	slackComposerFactory.On("SlackCompose", mock.Anything).Return(slackMock)
	testTearDown, router := setupHandlerTest(t, routeDep, EventHandler,
		WithSlackComposerFactory(slackComposerFactory),
		WithConfig(&config.Config{}),
	)
	defer testTearDown(t)

	// Setup test request
	var buf bytes.Buffer
	_, err := buf.Write([]byte(
		AMessageEvent),
	)
	assert.NoError(t, err)
	req, err := http.NewRequest("POST", "/", &buf)
	req.Header.Set("Content-Type", "application/json")
	assert.NoError(t, err)
	expectedResponseBody := map[string]string{"status": "RECEIVED"}

	// Make test request
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	var actualResponse map[string]string
	err = json.Unmarshal(w.Body.Bytes(), &actualResponse)
	assert.NoError(t, err)
	// assert response
	assert.Equal(t, http.StatusOK, w.Code, "HTTP code does not match expected")
	assert.Equal(t, expectedResponseBody, actualResponse, "response body does not match expected")
}

func TestHandleEventAsyncResponse_Failures(t *testing.T) {
	tests := []struct {
		name                  string
		parsedEvent           *slack.ParsedEvent
		mockCalls             func(routeDep *handlers.RouteDependencies)
		expectedErrorContains string
	}{
		{
			name: "Failed Getting channel config from DB",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("a channel config error"))
			},
			expectedErrorContains: "a channel config error",
		},
		{
			name: "no channel config in store",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
			},
			expectedErrorContains: "not found",
		},
		{
			name: "failed classification",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(false, errors.New("a question classification error"))
			},
			expectedErrorContains: "a question classification error",
		},
		{
			name: "failed to get team config from store",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(nil, errors.New("a team config error"))
			},
			expectedErrorContains: "a team config error",
		},
		{
			name: "no team config in store",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(nil, nil)
			},
			expectedErrorContains: "not found",
		},
		{
			name: "failed to extract search keywords",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return(nil, errors.New("an extraction error"))
			},
			expectedErrorContains: "an extraction error",
		},
		{
			name: "search key words are less than 2",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{""}, nil)
			},
			expectedErrorContains: "not enough",
		},
		{
			name: "channel config does not contain gitlab config",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{"", ""}, nil)
			},
			expectedErrorContains: "gitlab config not found",
		},
		{
			name: "failure to establish connection with gitlab",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{GitlabConfig: &models.GitlabConfig{}}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{"", ""}, nil)
				gitlabClient := gitlab_mocks.NewGitLabClient(t)
				routeDep.GitlabClientFactory.(*gitlab_mocks.GitLabClientFactory).On("NewClient", mock.Anything).Return(gitlabClient, errors.New("gitlab client istatiate error"))
			},
			expectedErrorContains: "gitlab client istatiate error",
		},
		{
			name: "failure to establish connection with gitlab",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{GitlabConfig: &models.GitlabConfig{}}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{"", ""}, nil)
				gitlabClient := gitlab_mocks.NewGitLabClient(t)
				routeDep.GitlabClientFactory.(*gitlab_mocks.GitLabClientFactory).On("NewClient", mock.Anything).Return(gitlabClient, errors.New("gitlab client istatiate error"))
			},
			expectedErrorContains: "gitlab client istatiate error",
		},
		{
			name: "failure to get gitlab issues for key word",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{GitlabConfig: &models.GitlabConfig{}}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{"", ""}, nil)
				gitlabClient := gitlab_mocks.NewGitLabClient(t)
				routeDep.GitlabClientFactory.(*gitlab_mocks.GitLabClientFactory).On("NewClient", mock.Anything).Return(gitlabClient, nil)
				gitlabClient.On("GetIssuesWithKeyWords", mock.Anything, mock.Anything).Return(nil, errors.New("an error getting gitlab issues"))
			},
			expectedErrorContains: "an error getting gitlab issues",
		},
		{
			name: "no gitlab issues found for keyword",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{GitlabConfig: &models.GitlabConfig{}}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{"", ""}, nil)
				gitlabClient := gitlab_mocks.NewGitLabClient(t)
				routeDep.GitlabClientFactory.(*gitlab_mocks.GitLabClientFactory).On("NewClient", mock.Anything).Return(gitlabClient, nil)
				gitlabClient.On("GetIssuesWithKeyWords", mock.Anything, mock.Anything).Return(nil, nil)
			},
			expectedErrorContains: "not found",
		},
		{
			name: "failure to post to slack",
			parsedEvent: &slack.ParsedEvent{
				Type: slack.CallbackEventKey,
				InnerEvent: slack.ParsedInnerEvent{
					Data: &slack.MessageEvent{
						BotID: "",
					},
				},
			},
			mockCalls: func(routeDep *handlers.RouteDependencies) {
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{GitlabConfig: &models.GitlabConfig{}}, nil)
				routeDep.Qd.(*qd_mocks.QuestionDetector).On("Classify", mock.Anything).Return(true, nil)
				routeDep.ConfigStore.(*cs_mocks.ConfigStore).On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
				routeDep.KeyEx.(*keyword_mock.KeyWordExtractor).On("Extract", mock.Anything, mock.Anything).Return([]string{"", ""}, nil)
				gitlabClient := gitlab_mocks.NewGitLabClient(t)
				routeDep.GitlabClientFactory.(*gitlab_mocks.GitLabClientFactory).On("NewClient", mock.Anything).Return(gitlabClient, nil)
				gitlabClient.On("GetIssuesWithKeyWords", mock.Anything, mock.Anything).Return([]gitlab.Issue{{}}, nil)
				slackComposer := slack_mock.NewSlack(t)
				routeDep.SlackComposerFactory.(*slack_mock.SlackFactory).On("SlackCompose", mock.Anything).Return(slackComposer)
				slackComposer.On("PostAttachmentWithNewClient", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("a slack post error"))
			},
			expectedErrorContains: "a slack post error",
		},
	}
	for _, test := range tests {
		// setup Mock
		keyExMock := keyword_mock.NewKeyWordExtractor(t)
		slackComposerFactory := slack_mock.NewSlackFactory(t)
		configstoreMock := cs_mocks.NewConfigStore(t)
		qdMock := qd_mocks.NewQuestionDetector(t)
		gitlabClientFactory := gitlab_mocks.NewGitLabClientFactory(t)
		test := test
		t.Run(test.name, func(t *testing.T) {
			var routeDep = &handlers.RouteDependencies{}
			testTearDown := setupTest(t, routeDep,
				WithConfig(&config.Config{}),
				WithQuestionDecider(qdMock),
				WithKeyWordExtractor(keyExMock),
				WithSlackComposerFactory(slackComposerFactory),
				WithGitlabClientFactory(gitlabClientFactory),
				WithConfigStore(configstoreMock),
				WithStdLogger,
			)
			defer testTearDown(t)
			test.mockCalls(routeDep)
			t.Parallel()
			actualError := handleEventAsyncResponse(*routeDep, test.parsedEvent)
			assert.ErrorContains(t, actualError, test.expectedErrorContains, "error does not match expected")
		})
	}
}

func TestHandleEventAsyncResponse_success(t *testing.T) {
	// Basic Setup: Adding logger
	suiteTearDown, routeDep := setupSuite(t, WithStdLogger)
	defer suiteTearDown(t)

	// Advance Setup: Configuring mocks

	// Key extractor Mock
	keyExMock := keyword_mock.NewKeyWordExtractor(t)
	keyExMock.On("Extract", mock.Anything, mock.Anything).
		Return([]string{"anything", "anything"}, nil)
	// Slack composer Mock
	slackComposerFactory := slack_mock.NewSlackFactory(t)
	slackMock := slack_mock.NewSlack(t)
	slackMock.On("PostAttachmentWithNewClient", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	slackComposerFactory.On("SlackCompose", mock.Anything).Return(slackMock)
	// Config store Mock
	configstoreMock := cs_mocks.NewConfigStore(t)
	configstoreMock.On("GetTeamConfig", mock.Anything, mock.Anything).Return(&models.OAuthResponse{}, nil)
	configstoreMock.On("GetChannelConfig", mock.Anything, mock.Anything, mock.Anything).Return(&models.ChannelConfig{GitlabConfig: &models.GitlabConfig{}}, nil)
	// Question decider Mock
	qdMock := qd_mocks.NewQuestionDetector(t)
	qdMock.On("Classify", mock.Anything).Return(true, nil)
	// GitLab client Mock
	gitlabClientFactory := gitlab_mocks.NewGitLabClientFactory(t)
	gitlabClient := gitlab_mocks.NewGitLabClient(t)
	gitlabClient.On("GetIssuesWithKeyWords", mock.Anything, mock.Anything).Return([]gitlab.Issue{{}, {}}, nil)
	gitlabClientFactory.On("NewClient", mock.Anything).Return(gitlabClient, nil)

	testTearDown := setupTest(t, routeDep,
		WithConfig(&config.Config{}),
		WithQuestionDecider(qdMock),
		WithKeyWordExtractor(keyExMock),
		WithSlackComposerFactory(slackComposerFactory),
		WithGitlabClientFactory(gitlabClientFactory),
		WithConfigStore(configstoreMock),
	)
	defer testTearDown(t)
	parsedEvent := &slack.ParsedEvent{
		Type: slack.CallbackEventKey,
		InnerEvent: slack.ParsedInnerEvent{
			Data: &slack.MessageEvent{
				BotID: "",
			},
		},
	}
	actualError := handleEventAsyncResponse(*routeDep, parsedEvent)
	assert.Equal(t, nil, actualError, "error does not match expected")
}
