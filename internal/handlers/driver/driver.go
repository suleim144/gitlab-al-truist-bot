package driver

import (
	"context"

	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
)

type ChannelStorage interface {
	UpsertChannelConfig(ctx context.Context, teamId string, channelId string, config *models.ChannelConfig) error
	GetChannelConfig(ctx context.Context, teamId, channelId string) (*models.ChannelConfig, error)
}
