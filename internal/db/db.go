package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoConfigDB struct {
	MongoClient             *mongo.Client
	ChannelConfigCollection *mongo.Collection
	TeamConfigCollection    *mongo.Collection
}

func NewMongodb(cfg *config.Config, log logger.Logger) (*MongoConfigDB, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	serverAPIOptions := options.ServerAPI(options.ServerAPIVersion1)
	clientOptions := options.Client().
		ApplyURI(cfg.DBScheme + cfg.DBUsername + ":" + cfg.DBPassword + "@" + cfg.DBHost + "/?retryWrites=true&w=majority").
		SetServerAPIOptions(serverAPIOptions)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}
	ctx, cancel = context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}
	teamConfigCollection := client.Database("backend").Collection("team")
	channelConfigCollection := client.Database("backend").Collection("channelConfig")
	return &MongoConfigDB{client, channelConfigCollection, teamConfigCollection}, nil
}

func (mgcreds *MongoConfigDB) GetTeamConfig(ctx context.Context, teamID string) (*models.Team, error) {
	tc := models.Team{}
	filter := bson.D{{Key: "id", Value: teamID}}
	err := mgcreds.TeamConfigCollection.FindOne(ctx, filter).Decode(&tc)
	if err == mongo.ErrNoDocuments {
		return nil, errors.New("team was not found")
	} else if err != nil {
		return nil, err
	}
	return &tc, nil
}

func (mgcreds *MongoConfigDB) GetChannelConfig(ctx context.Context, teamID string, channelID string) (*models.Channel, error) {
	ch := models.Channel{}
	filter := bson.D{{Key: "teamid", Value: teamID}, {Key: "channelid", Value: channelID}}
	err := mgcreds.ChannelConfigCollection.FindOne(ctx, filter).Decode(&ch)
	if err == mongo.ErrNoDocuments {
		return nil, errors.New("channel not found")
	} else if err != nil {
		return nil, err
	}
	return &ch, nil
}
func (mgcreds *MongoConfigDB) UpsertTeamConfig(ctx context.Context, teamID string, config *models.OAuthResponse) error {
	filter := bson.D{{Key: "id", Value: teamID}}
	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "config", Value: config},
		}},
	}
	opts := options.Update().SetUpsert(true)
	_, err := mgcreds.TeamConfigCollection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		return err
	}
	return nil
}

func (mgcreds *MongoConfigDB) UpsertChannelConfig(ctx context.Context, teamID string, channelID string, config *models.ChannelConfig) error {
	filter := bson.D{{Key: "teamid", Value: teamID}, {Key: "channelid", Value: channelID}}
	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "config", Value: config},
		}},
	}
	opts := options.Update().SetUpsert(true)
	_, err := mgcreds.ChannelConfigCollection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		return err
	}
	return nil
}
