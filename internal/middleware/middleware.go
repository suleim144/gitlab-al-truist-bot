package miiddleware

import (
	"context"
	"net/http/httputil"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
)

func SetRequestContext(requestTimeout time.Duration) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cncl := context.WithTimeout(c.Request.Context(), requestTimeout)
		c.Request = c.Request.WithContext(ctx)
		c.Set("cancelReq", cncl)
	}
}

func RequestDumper(log logger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestByte, err := httputil.DumpRequest(c.Request, true)
		if err != nil {
			log.Warnf("error dumping request: %s", err)
		}
		log.Infof("request: %s \n", string(requestByte[:]))
	}
}

func IgnoreCORS() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
