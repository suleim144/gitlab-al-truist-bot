package classifier

import (
	"context"

	"github.com/nlpodyssey/cybertron/pkg/tasks"
	"github.com/nlpodyssey/cybertron/pkg/tasks/textclassification"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
)

type QuestionDetector interface {
	Classify(text string) (bool, error)
}

type QuestionClassifier struct {
	m textclassification.Interface
}

func (qd *QuestionClassifier) Shutdown() {
	tasks.Finalize(qd.m)
}

func (qd *QuestionClassifier) Classify(text string) (bool, error) {
	result, err := qd.m.Classify(context.Background(), text)
	if err != nil {
		return false, err
	}
	for index, score := range result.Scores {
		if score > 0.5 {
			if result.Labels[index] == "question" {
				return true, nil
			} else {
				return false, nil
			}
		}
	}
	return false, err
}

func LoadNLPQuestionProcessor(cfg *config.Config, log logger.Logger) (*QuestionClassifier, error) {
	m, err := tasks.Load[textclassification.Interface](&tasks.Config{ModelsDir: cfg.ModelsDir, ModelName: cfg.ModelName})
	if err != nil {
		return nil, err
	}
	return &QuestionClassifier{
		m: m,
	}, nil
}
