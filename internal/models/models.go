package models

type Channel struct {
	ChannelID string         `json:"channel_id" bson:"channelid"`
	TeamID    string         `json:"team_id" bson:"teamid"`
	Config    *ChannelConfig `json:"config" bson:"config"`
}
type ChannelConfig struct {
	ChannelID    string        `json:"channel_id" bson:"channelid"`
	ChannelName  string        `json:"channel_name" bson:"channelname"`
	TeamID       string        `json:"team_id" bson:"teamid"`
	TeamDomain   string        `json:"team_domain" bson:"teamdomain"`
	UserID       string        `json:"user_id" bson:"userid"`
	UserName     string        `json:"user_name" bson:"username"`
	GitlabConfig *GitlabConfig `json:"gitlab" bson:"gitlabconfig"`
}

type GitlabConfig struct {
	ProjectID   string `json:"projectid"`
	AccessToken string `json:"accesstoken"`
}

type CmdEventMeta struct {
	ChannelID   string `json:"channel_id" bson:"channelid"`
	ChannelName string `json:"channel_name" bson:"channelname"`
	TeamID      string `json:"team_id" bson:"teamid"`
	TeamDomain  string `json:"team_domain" bson:"teamdomain"`
	UserID      string `json:"user_id" bson:"userid"`
	UserName    string `json:"user_name" bson:"username"`
}

type GalArgs struct {
	ProjectID   string `json:"searchProject"`
	AccessToken string `json:"useToken"`
}

type Team struct {
	Id     string         `json:"id" bson:"id"`
	Config *OAuthResponse `json:"config" bson:"config"`
}

type OAuthResponse struct {
	AccessToken     string                       `json:"access_token"`
	TokenType       string                       `json:"token_type"`
	Scope           string                       `json:"scope"`
	BotUserID       string                       `json:"bot_user_id"`
	AppID           string                       `json:"app_id"`
	Team            OAuthResponseTeam            `json:"team"`
	IncomingWebhook OAuthResponseIncomingWebhook `json:"incoming_webhook"`
	Enterprise      OAuthResponseEnterprise      `json:"enterprise"`
	AuthedUser      OAuthResponseAuthedUser      `json:"authed_user"`
	RefreshToken    string                       `json:"refresh_token"`
	ExpiresIn       int                          `json:"expires_in"`
}

type OAuthResponseTeam struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type OAuthResponseIncomingWebhook struct {
	URL              string `json:"url"`
	Channel          string `json:"channel"`
	ChannelID        string `json:"channel_id,omitempty"`
	ConfigurationURL string `json:"configuration_url"`
}

type OAuthResponseEnterprise struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type OAuthResponseAuthedUser struct {
	ID           string `json:"id"`
	Scope        string `json:"scope"`
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
}
