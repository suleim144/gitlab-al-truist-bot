// Code generated by mockery v2.15.0. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	slack "gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

// SlackFactory is an autogenerated mock type for the SlackFactory type
type SlackFactory struct {
	mock.Mock
}

// SlackCompose provides a mock function with given fields: slackCredentials
func (_m *SlackFactory) SlackCompose(slackCredentials *slack.SlackCredentials) slack.Slack {
	ret := _m.Called(slackCredentials)

	var r0 slack.Slack
	if rf, ok := ret.Get(0).(func(*slack.SlackCredentials) slack.Slack); ok {
		r0 = rf(slackCredentials)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(slack.Slack)
		}
	}

	return r0
}

type mockConstructorTestingTNewSlackFactory interface {
	mock.TestingT
	Cleanup(func())
}

// NewSlackFactory creates a new instance of SlackFactory. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewSlackFactory(t mockConstructorTestingTNewSlackFactory) *SlackFactory {
	mock := &SlackFactory{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
