package logger

import (
	"go.uber.org/zap"
)

type zapLogger struct {
	log *zap.Logger
}

func NewZapLogger(debugMode bool) (Logger, error) {
	var logger *zap.Logger
	var err error
	options := []zap.Option{zap.AddCaller(), zap.AddCallerSkip(1)}
	if !debugMode {
		logger, err = zap.NewProduction(options...)
	} else {
		logger, err = zap.NewDevelopment(options...)
	}
	zap.ReplaceGlobals(logger)
	return &zapLogger{
		logger,
	}, err
}

func (logger *zapLogger) Info(params ...interface{}) {
	logger.log.Sugar().Info(params)
}

func (logger *zapLogger) Infof(arg string, params ...interface{}) {
	logger.log.Sugar().Infof(arg, params...)
}

func (logger *zapLogger) Infow(arg string, params ...interface{}) {
	logger.log.Sugar().Infow(arg, params...)
}

func (logger *zapLogger) Warn(params ...interface{}) {
	logger.log.Sugar().Warn(params)
}

func (logger zapLogger) Warnf(arg string, params ...interface{}) {
	logger.log.Sugar().Warnf(arg, params...)
}

func (logger *zapLogger) Warnw(arg string, params ...interface{}) {
	logger.log.Sugar().Warnw(arg, params...)
}

func (logger *zapLogger) Debug(params ...interface{}) {
	logger.log.Sugar().Debug(params)
}

func (logger *zapLogger) Debugf(arg string, params ...interface{}) {
	logger.log.Sugar().Infof(arg, params...)
}

func (logger *zapLogger) Debugw(arg string, params ...interface{}) {
	logger.log.Sugar().Debugw(arg, params...)
}

func (logger *zapLogger) Fatal(params ...interface{}) {
	logger.log.Sugar().Fatal(params)
}

func (logger *zapLogger) Fatalf(arg string, params ...interface{}) {
	logger.log.Sugar().Fatalf(arg, params...)
}

func (logger *zapLogger) Fatalw(arg string, params ...interface{}) {
	logger.log.Sugar().Fatalw(arg, params...)
}

func (logger *zapLogger) Panic(params ...interface{}) {
	logger.log.Sugar().Panic(params)
}

func (logger zapLogger) Panicf(arg string, params ...interface{}) {
	logger.log.Sugar().Fatalf(arg, params...)
}

func (logger *zapLogger) Panicw(arg string, params ...interface{}) {
	logger.log.Sugar().Panicw(arg, params...)
}

func (logger *zapLogger) Shutdown() {
	logger.log.Sync()
}
