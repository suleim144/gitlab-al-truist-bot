package logger

import (
	"log"
)

type stdLogger struct {
	log   *log.Logger
	debug bool
}

func NewStdLogger(debugMode bool) Logger {
	return &stdLogger{
		log:   log.Default(),
		debug: debugMode,
	}
}

func (logger *stdLogger) Info(params ...interface{}) {
	logger.log.Print(params...)
}

func (logger *stdLogger) Infof(arg string, params ...interface{}) {
	logger.log.Printf(arg, params...)
}

func (logger *stdLogger) Infow(arg string, params ...interface{}) {
	logger.log.Printf(arg, params...)
}

func (logger *stdLogger) Warn(params ...interface{}) {
	logger.log.Print(params...)
}

func (logger *stdLogger) Warnf(arg string, params ...interface{}) {
	logger.log.Printf(arg, params...)
}

func (logger *stdLogger) Warnw(arg string, params ...interface{}) {
	logger.log.Printf(arg, params...)
}

func (logger *stdLogger) Debug(params ...interface{}) {
	if logger.debug {
		logger.log.Print(params...)
	}
}

func (logger *stdLogger) Debugf(arg string, params ...interface{}) {
	if logger.debug {
		logger.log.Printf(arg, params...)
	}
}

func (logger *stdLogger) Debugw(arg string, params ...interface{}) {
	if logger.debug {
		logger.log.Printf(arg, params...)
	}
}

func (logger *stdLogger) Fatal(params ...interface{}) {
	logger.log.Fatal(params...)
}

func (logger *stdLogger) Fatalf(arg string, params ...interface{}) {
	logger.log.Fatalf(arg, params...)
}

func (logger *stdLogger) Fatalw(arg string, params ...interface{}) {
	logger.log.Fatalf(arg, params...)
}

func (logger *stdLogger) Panic(params ...interface{}) {
	logger.log.Panic(params...)
}

func (logger *stdLogger) Panicf(arg string, params ...interface{}) {
	logger.log.Panicf(arg, params...)
}

func (logger *stdLogger) Panicw(arg string, params ...interface{}) {
	logger.log.Panicf(arg, params...)
}

func (logger *stdLogger) Shutdown() {}
