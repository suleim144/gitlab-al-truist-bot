package logger

type Logger interface {
	Info(...interface{})
	Debug(...interface{})
	Warn(...interface{})
	Panic(...interface{})
	Fatal(...interface{})
	Infof(string, ...interface{})
	Warnf(string, ...interface{})
	Fatalf(string, ...interface{})
	Panicf(string, ...interface{})
	Debugf(string, ...interface{})

	// The structured logging functions below are only
	// supported by some logger types (e.g zap)
	Infow(msg string, keysAndValues ...interface{})
	Warnw(msg string, keysAndValues ...interface{})
	Fatalw(msg string, keysAndValues ...interface{})
	Panicw(msg string, keysAndValues ...interface{})
	Debugw(msg string, keysAndValues ...interface{})

	Shutdown()
}
