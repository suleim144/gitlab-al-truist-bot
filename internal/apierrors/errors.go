package apierrors

import (
	"errors"
	"net/http"

	"schneider.vip/problem"
)

type APIError interface {
	// APIError returns an HTTP status code and an API-safe error message.
	APIError() (int, string, string)
}

type ResponseAPIError struct {
	status int
	msg    string
	detail string
}

func (e ResponseAPIError) Error() string {
	return e.msg
}

func (e ResponseAPIError) APIError() (int, string, string) {
	return e.status, e.msg, e.detail
}

var (
	ErrBadRequest       = &ResponseAPIError{status: http.StatusBadRequest, msg: "bad reequest"}
	ErrInternalError    = &ResponseAPIError{status: http.StatusInternalServerError, msg: "internal error"}
	ErrNotModifiedError = &ResponseAPIError{status: http.StatusNotModified, msg: "resource was not modified"}
	ErrWrongAccess      = &ResponseAPIError{status: http.StatusForbidden, msg: "forbidden"}
)

type wrappedResponseAPIError struct {
	error
	responseAPIError *ResponseAPIError
}

func (e wrappedResponseAPIError) Is(err error) bool {
	return e.responseAPIError == err
}

func (e wrappedResponseAPIError) APIError() (int, string, string) {
	return e.responseAPIError.APIError()
}

func WrapError(err error, respErr *ResponseAPIError) error {
	respErr.detail = err.Error()
	return wrappedResponseAPIError{error: err, responseAPIError: respErr}
}

func JSONHandleError(w http.ResponseWriter, err error) {
	var apiErr APIError
	if errors.As(err, &apiErr) {
		status, msg, detail := apiErr.APIError()
		JSONError(w, status, msg, detail)
	} else {
		JSONError(w, http.StatusInternalServerError, "internal error", "")
	}
}

func JSONError(w http.ResponseWriter, status int, msg string, detail string) {
	problem.New(
		problem.Title(msg),
		problem.Status(status),
		problem.Detail(detail),
	).WriteTo(w)
}
