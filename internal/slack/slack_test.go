package slack

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var AMessageEvent = `{
	"token":"pJhDiPvEcbK5Lq4HbQI2hvC8",
	"team_id":"T02B8CZ64LB",
	"context_team_id":"T02B8CZ64LB",
	"context_enterprise_id":null,
	"api_app_id":"A04NPT8EDQ8",
	"event":{
		"client_msg_id":"af1e950e-9ae9-426d-b812-c02e8f2ba6d8",
		"type":"message",
		"text":"hey this is a question?",
		"user":"U02BEPQUWKE",
		"ts":"1675707637.048429",
		"blocks":[
			{
				"type":"rich_text",
				"block_id":"ja=",
				"elements":[
					{
						"type":"rich_text_section",
						"elements":[
							{
								"type":"text",
								"text":"hey this is a question?"
							}
						]
					}
				]
			}
		],
		"team":"T02B8CZ64LB",
		"channel":"C04N9MA53FD",
		"event_ts":"1675707637.048429",
		"channel_type":"channel"
	},
	"type":"event_callback",
	"event_id":"Ev04N6TBFJNR",
	"event_time":1675707637,
	"authorizations":[
		{
			"enterprise_id":null,
			"team_id":"T02B8CZ64LB",
			"user_id":"U04NQ5B7W56",
			"is_bot":true,
			"is_enterprise_install":false
		}
	],
	"is_ext_shared_channel":false,
	"event_context":"4-eyJldCI6Im1lc3NhZ2UiLCJ0aWQiOiJUMDJCOENaNjRMQiIsImFpZCI6IkEwNE5QVDhFRFE4IiwiY2lkIjoiQzA0TjlNQTUzRkQifQ"
}`

func TestSuccesfulParseCmdText(t *testing.T) {
	queryString := "     searchProject=deviceIssues          useToken=xv234ce "
	expectedQueryParams := map[string]string{
		"searchProject": "deviceIssues",
		"useToken":      "xv234ce",
	}
	cmdParams := parseCmdArgs(queryString)
	assert.Equal(t, expectedQueryParams, cmdParams)
}

func TestExtractChallange(t *testing.T) {
	p := &ParsedEvent{
		Data: URLVerificationEvent{
			Challenge: "blablabla",
		},
	}
	challange, err := p.ExtractChallange()
	assert.Equal(t, "blablabla", challange)
	assert.Empty(t, err)
}

func TestExtractChallange_Error(t *testing.T) {
	p := &ParsedEvent{
		Data: "blablabla",
	}
	challange, err := p.ExtractChallange()
	assert.Empty(t, challange)
	assert.NotEmpty(t, err)
}

func TestParseEvent(t *testing.T) {
	expectedParsedEvent := &ParsedEvent{
		Token:    "pJhDiPvEcbK5Lq4HbQI2hvC8",
		Data:     &EventsAPICallbackEvent{},
		TeamID:   "T02B8CZ64LB",
		Type:     CallbackEventKey,
		APIAppID: "A04NPT8EDQ8",
		InnerEvent: ParsedInnerEvent{
			Type: "message",
			Data: &MessageEvent{
				Text:    "hey this is a question?",
				Channel: "C04N9MA53FD",
			},
		},
	}
	actualParsedEvent, err := EventParseFunc([]byte(AMessageEvent))
	assert.ObjectsAreEqual(expectedParsedEvent, actualParsedEvent)
	assert.Nil(t, err)
}
