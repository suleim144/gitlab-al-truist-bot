package slack

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/apierrors"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/models"
)

type ParsedCmd struct {
	Cmd       string
	Args      map[string]string
	EventMeta *models.CmdEventMeta
}
type ParsedEvent struct {
	Token        string `json:"token"`
	TeamID       string `json:"team_id"`
	Type         string `json:"type"`
	APIAppID     string `json:"api_app_id"`
	EnterpriseID string `json:"enterprise_id"`
	Data         interface{}
	InnerEvent   ParsedInnerEvent
}
type URLVerificationEvent struct {
	Token     string `json:"token"`
	Challenge string `json:"challenge"`
	Type      string `json:"type"`
}
type EventsAPICallbackEvent struct {
	Type         string           `json:"type"`
	Token        string           `json:"token"`
	TeamID       string           `json:"team_id"`
	APIAppID     string           `json:"api_app_id"`
	EnterpriseID string           `json:"enterprise_id"`
	InnerEvent   *json.RawMessage `json:"event"`
	AuthedUsers  []string         `json:"authed_users"`
	AuthedTeams  []string         `json:"authed_teams"`
	EventID      string           `json:"event_id"`
	EventTime    int              `json:"event_time"`
	EventContext string           `json:"event_context"`
}
type ParsedInnerEvent struct {
	Type string `json:"type"`
	Data interface{}
}

type CmdQueryParams struct {
	SearchProject string
	UseToken      string
}
type SlackFactory interface {
	SlackCompose(slackCredentials *SlackCredentials) Slack
}

type RealSlackFactory struct{}

func (rsf RealSlackFactory) SlackCompose(slackCredentials *SlackCredentials) Slack {
	return NewSlackComposer(slackCredentials)
}

type Slack interface {
	VerifySignature(header http.Header, body []byte) (bool, error)
	ParseCmd(request *http.Request) (*ParsedCmd, error)
	ParseEvent(event []byte) (*ParsedEvent, error)
	GetOAuth2Token(ctx context.Context, code string, redirectURI string) (*models.OAuthResponse, error)
	SlackResponderWithNewClient
}

type sigVer func(slackSigningSecret string, header http.Header, body []byte) (bool, error)
type cmdParse func(request *http.Request) (*ParsedCmd, error)
type oauth2Exchange func(ctx context.Context, client *http.Client, clientID string, clientSecret string, code string, redirectURI string) (*models.OAuthResponse, error)
type eventParse func(event []byte) (*ParsedEvent, error)

type SlackComposer struct {
	OAuthclient               *http.Client
	slackCredentials          *SlackCredentials
	signatureVerificationFunc sigVer
	cmdParseFunc              cmdParse
	oauth2ExchangeFunc        oauth2Exchange
	eventParseFunc            eventParse
	SlackResponderWithNewClient
}

type SlackCredentials struct {
	SlackSigningSecret string
	SlackClientID      string
	SlackClientSecret  string
}

func NewSlackComposer(slackCredentials *SlackCredentials) *SlackComposer {
	return &SlackComposer{
		OAuthclient:                 &http.Client{},
		slackCredentials:            slackCredentials,
		signatureVerificationFunc:   SignatureVerificationFunc,
		cmdParseFunc:                CmdParseFunc,
		oauth2ExchangeFunc:          Oauth2ExchangeFunc,
		eventParseFunc:              EventParseFunc,
		SlackResponderWithNewClient: &SlackResponderClient{},
	}
}

func (scomposer *SlackComposer) VerifySignature(header http.Header, body []byte) (bool, error) {
	if scomposer.slackCredentials != nil {
		return scomposer.signatureVerificationFunc(scomposer.slackCredentials.SlackSigningSecret, header, body)
	} else {
		return false, errors.New("slackComposer does not contain contain a slackCredentials object")
	}

}

func (scomposer *SlackComposer) ParseEvent(event []byte) (*ParsedEvent, error) {
	return scomposer.eventParseFunc(event)
}

func (scomposer *SlackComposer) ParseCmd(request *http.Request) (*ParsedCmd, error) {
	return scomposer.cmdParseFunc(request)
}

func (scomposer *SlackComposer) GetOAuth2Token(ctx context.Context, code string, redirectURI string) (*models.OAuthResponse, error) {
	if scomposer.slackCredentials != nil {
		return scomposer.oauth2ExchangeFunc(ctx, scomposer.OAuthclient, scomposer.slackCredentials.SlackClientID, scomposer.slackCredentials.SlackClientSecret, code, redirectURI)
	} else {
		return nil, errors.New("slackComposer does not contain contain a slackCredentials object")
	}
}

func Oauth2ExchangeFunc(ctx context.Context, OAuthclient *http.Client, slackClientID, slackClientSecret, code, redirectURI string) (*models.OAuthResponse, error) {
	// exchange token
	OAuthResponse, err := slack.GetOAuthV2ResponseContext(ctx, OAuthclient, slackClientID, slackClientSecret, code, redirectURI)
	if err != nil || OAuthResponse == nil {
		return nil, fmt.Errorf("failed to exchange code for token: %s", err)
	}

	return unpackOAuthResponse(OAuthResponse), nil
}

func CmdParseFunc(request *http.Request) (*ParsedCmd, error) {

	// parse params
	scmd, err := slack.SlashCommandParse(request)
	if err != nil {
		return nil, fmt.Errorf("could not parse the slack command: %v", err)
	}

	args := parseCmdArgs(scmd.Text)

	eventMeta := parseEventMeta(scmd)

	return &ParsedCmd{
		scmd.Command,
		args,
		eventMeta,
	}, nil
}

func EventParseFunc(event []byte) (*ParsedEvent, error) {

	// parse params
	EventsAPIEvent, err := slackevents.ParseEvent(json.RawMessage(event), slackevents.OptionNoVerifyToken())
	if err != nil {
		return nil, fmt.Errorf("could not parse the slack command: %v", err)
	}

	return unpackEventsAPIEvent(EventsAPIEvent), nil
}

func SignatureVerificationFunc(signingSecret string, header http.Header, body []byte) (bool, error) {
	verifier, err := slack.NewSecretsVerifier(header, signingSecret)
	if err != nil {
		return false, err
	}
	if _, err := verifier.Write(body); err != nil {
		err = apierrors.WrapError(err, apierrors.ErrInternalError)
		return false, err
	}
	if err = verifier.Ensure(); err != nil {
		return false, err
	}
	return true, nil
}

func parseCmdArgs(queryString string) map[string]string {
	args := map[string]string{}
	for _, pair := range strings.Split(queryString, " ") {
		kv := strings.Split(pair, "=")
		if len(kv) == 2 {
			args[kv[0]] = kv[1]
		}
	}
	return args
}

func parseEventMeta(scmd slack.SlashCommand) *models.CmdEventMeta {

	cmdMeta := &models.CmdEventMeta{
		ChannelID:   scmd.ChannelID,
		ChannelName: scmd.ChannelName,
		TeamID:      scmd.TeamID,
		TeamDomain:  scmd.TeamDomain,
		UserID:      scmd.UserID,
		UserName:    scmd.UserName,
	}
	// unpack
	return cmdMeta

}

func unpackOAuthResponse(OAuthResponse *slack.OAuthV2Response) *models.OAuthResponse {
	return &models.OAuthResponse{
		AccessToken: OAuthResponse.AccessToken,
		TokenType:   OAuthResponse.TokenType,
		Scope:       OAuthResponse.Scope,
		BotUserID:   OAuthResponse.BotUserID,
		AppID:       OAuthResponse.AppID,
		Team: models.OAuthResponseTeam{
			ID:   OAuthResponse.Team.ID,
			Name: OAuthResponse.Team.Name,
		},
		IncomingWebhook: models.OAuthResponseIncomingWebhook{
			URL:              OAuthResponse.IncomingWebhook.URL,
			Channel:          OAuthResponse.IncomingWebhook.Channel,
			ChannelID:        OAuthResponse.IncomingWebhook.ChannelID,
			ConfigurationURL: OAuthResponse.IncomingWebhook.ConfigurationURL,
		},
		Enterprise: models.OAuthResponseEnterprise{
			ID:   OAuthResponse.Enterprise.ID,
			Name: OAuthResponse.Enterprise.Name,
		},
		AuthedUser: models.OAuthResponseAuthedUser{
			ID:           OAuthResponse.AuthedUser.ID,
			Scope:        OAuthResponse.AuthedUser.Scope,
			AccessToken:  OAuthResponse.AuthedUser.AccessToken,
			ExpiresIn:    OAuthResponse.AuthedUser.ExpiresIn,
			RefreshToken: OAuthResponse.AuthedUser.RefreshToken,
			TokenType:    OAuthResponse.AuthedUser.TokenType,
		},
		RefreshToken: OAuthResponse.RefreshToken,
		ExpiresIn:    OAuthResponse.ExpiresIn,
	}
}

func unpackEventsAPIEvent(eventsAPIEvent slackevents.EventsAPIEvent) *ParsedEvent {
	parsedEvent := &ParsedEvent{
		Token:        eventsAPIEvent.Token,
		TeamID:       eventsAPIEvent.TeamID,
		Type:         eventsAPIEvent.Type,
		APIAppID:     eventsAPIEvent.APIAppID,
		EnterpriseID: eventsAPIEvent.EnterpriseID,
		Data:         eventsAPIEvent.Data,
		InnerEvent: ParsedInnerEvent{
			eventsAPIEvent.InnerEvent.Type,
			eventsAPIEvent.InnerEvent.Data,
		},
	}

	if eventsAPIEvent.Type == UrlVerificationEventKey {
		urlVerificationEvent, ok := unpackURLVerificationEventData(parsedEvent.Data)
		if ok {
			parsedEvent.Data = urlVerificationEvent
		}

	}
	if eventsAPIEvent.Type == CallbackEventKey {
		callBackEvent, ok := unpackCallbackEventData(parsedEvent.Data)
		if ok {
			parsedEvent.Data = callBackEvent
		}
	}

	if eventsAPIEvent.InnerEvent.Type == MessageInnerEventKey {
		innerMessageEvent, ok := unpackInnerMessageEventData(eventsAPIEvent.InnerEvent.Data)
		if ok {
			parsedEvent.InnerEvent.Data = innerMessageEvent
		}
	}

	return parsedEvent
}

func unpackCallbackEventData(data interface{}) (*EventsAPICallbackEvent, bool) {
	slacCallbackEvent, ok := data.(*slackevents.EventsAPICallbackEvent)
	if !ok {
		return nil, false
	}
	return &EventsAPICallbackEvent{
		Token:        slacCallbackEvent.Token,
		Type:         slacCallbackEvent.Type,
		TeamID:       slacCallbackEvent.TeamID,
		APIAppID:     slacCallbackEvent.APIAppID,
		EnterpriseID: slacCallbackEvent.EnterpriseID,
		InnerEvent:   slacCallbackEvent.InnerEvent,
		AuthedUsers:  slacCallbackEvent.AuthedUsers,
		AuthedTeams:  slacCallbackEvent.AuthedTeams,
		EventID:      slacCallbackEvent.EventID,
		EventTime:    slacCallbackEvent.EventTime,
		EventContext: slacCallbackEvent.EventContext,
	}, true
}

func unpackURLVerificationEventData(data interface{}) (*URLVerificationEvent, bool) {
	slackURLVerificationEvent, ok := data.(*slackevents.EventsAPIURLVerificationEvent)
	if !ok {
		return nil, false
	}
	return &URLVerificationEvent{
		Token:     slackURLVerificationEvent.Token,
		Type:      slackURLVerificationEvent.Type,
		Challenge: slackURLVerificationEvent.Challenge,
	}, true
}

func unpackInnerMessageEventData(data interface{}) (*MessageEvent, bool) {
	slackMessageEvent, ok := data.(*slackevents.MessageEvent)
	if !ok {
		return nil, false
	}
	return &MessageEvent{
		BotID:   slackMessageEvent.BotID,
		Text:    slackMessageEvent.Text,
		Channel: slackMessageEvent.Channel,
	}, true
}

var (
	UrlVerificationEventKey = slackevents.URLVerification
	CallbackEventKey        = slackevents.CallbackEvent
	MessageInnerEventKey    = string(slackevents.Message)
)

func (p *ParsedEvent) IsURLVerificationRequest() bool {
	return p.Type == UrlVerificationEventKey

}

func (p *ParsedEvent) IsACallBackEvent() bool {
	return p.Type == CallbackEventKey

}

func (p *ParsedEvent) ExtractChallange() (string, error) {
	urlVerificationEvent, ok := p.Data.(URLVerificationEvent)
	if !ok {
		return "", fmt.Errorf("failed to parse url_verification event")
	}
	return urlVerificationEvent.Challenge, nil
}

func (pi *ParsedInnerEvent) IsAMessageEvent() bool {
	//_, ok := pi.Data.(*slackevents.MessageEvent)
	_, ok := pi.Data.(*MessageEvent)
	return ok
}

func (pi *ParsedInnerEvent) GetMessageEvent() MessageEvent {
	var messageEvent MessageEvent
	//msgEvent, _ := pi.Data.(*slackevents.MessageEvent)
	msgEvent, _ := pi.Data.(*MessageEvent)
	messageEvent.Text = msgEvent.Text
	messageEvent.Channel = msgEvent.Channel
	messageEvent.BotID = msgEvent.BotID
	return messageEvent
}

type MessageEvent struct {
	BotID   string `json:"bot_id,omitempty"`
	Text    string `json:"text"`
	Channel string `json:"channel"`
}
