package slack

import (
	"github.com/slack-go/slack"
)

type SlackClient interface {
	SlackResponder
}

type SlackResponder interface {
	SlackResponderWithNewClient
	PostAttachment(channelID string, att []Attachment, text string) error
}

type SlackResponderWithNewClient interface {
	PostAttachmentWithNewClient(accessToken string, channelID string, att []Attachment, text string) error
}

type SlackResponderClient struct {
	client *slack.Client
}

type Attachment struct {
	Title     string
	TitleLink string
}

func NewSlackClientWithAccessToken(accessToken string) SlackClient {
	return &SlackResponderClient{slack.New(accessToken)}
}

func (srp *SlackResponderClient) PostAttachment(channelID string, att []Attachment, text string) error {
	_, _, err := srp.client.PostMessage(channelID, slack.MsgOptionCompose(
		slack.MsgOptionEnableLinkUnfurl(),
		slack.MsgOptionText(text, false),
		slack.MsgOptionAttachments(createSlackNativeAttachment(att)...)),
	)
	return err
}

func (srp *SlackResponderClient) PostAttachmentWithNewClient(accessToken string, channelID string, att []Attachment, text string) error {
	sc := &SlackResponderClient{slack.New(accessToken)}
	_, _, err := sc.client.PostMessage(channelID, slack.MsgOptionCompose(
		slack.MsgOptionEnableLinkUnfurl(),
		slack.MsgOptionText(text, false),
		slack.MsgOptionAttachments(createSlackNativeAttachment(att)...)),
	)
	return err
}

func createSlackNativeAttachment(atts []Attachment) []slack.Attachment {
	var slackAtt []slack.Attachment
	for _, att := range atts {
		slackAtt = append(slackAtt, slack.Attachment{
			Title:     att.Title,
			TitleLink: att.TitleLink,
		})
	}
	return slackAtt
}
