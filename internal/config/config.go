package config

import (
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	DebugMode                 bool
	Port                      string
	RequestTimeout            time.Duration
	IgnoreCORS                bool
	DBUsername                string
	DBPassword                string
	DBHost                    string
	DBScheme                  string
	ModelsDir                 string
	ModelName                 string
	KeywordExtractDefaultLang string
	SlackClientID             string
	SlackClientSecret         string
	SlackSigningfSecret       string
}

func LoadConfig() (*Config, error) {
	config := viper.New()
	// set a default for environment variables
	config.SetDefault("MODELS_DIR", "models")
	config.SetDefault("MODEL", "shahrukhx01/question-vs-statement-classifier")
	config.SetDefault("DEFAULT_KEYWORD_EXTRACTOR_LANG", "en")

	config.SetDefault("DEBUG_MODE", true)
	config.SetDefault("PORT", "80")
	config.SetDefault("REQUEST_TIMEOUT", "10s")
	config.SetDefault("IGNORE_CORS", false)
	// Load all envrionment variables

	config.SetDefault("DB_USERNAME", "root")
	config.SetDefault("DB_PASSWORD", "test1234")
	config.SetDefault("DB_HOST", "localhost")
	config.SetDefault("DB_SCHEME", "mongodb://")

	config.SetDefault("SLACK_CLIENT_SECRET", "secret")
	config.SetDefault("SLACK_CLIENT_ID", "clientID")
	config.SetDefault("SLACK_SIGNING_SECRET", "")

	config.AutomaticEnv()

	return &Config{
		DebugMode:                 config.GetBool("DEBUG_MODE"),
		Port:                      config.GetString("PORT"),
		RequestTimeout:            config.GetDuration("REQUEST_TIMEOUT"),
		IgnoreCORS:                config.GetBool("IGNORE_CORS"),
		DBUsername:                config.GetString("DB_USERNAME"),
		DBPassword:                config.GetString("DB_PASSWORD"),
		DBHost:                    config.GetString("DB_HOST"),
		DBScheme:                  config.GetString("DB_SCHEME"),
		ModelsDir:                 config.GetString("MODELS_DIR"),
		ModelName:                 config.GetString("MODEL"),
		KeywordExtractDefaultLang: config.GetString("DEFAULT_KEYWORD_EXTRACTOR_LANG"),
		SlackClientID:             config.GetString("SLACK_CLIENT_ID"),
		SlackClientSecret:         config.GetString("SLACK_CLIENT_SECRET"),
		SlackSigningfSecret:       config.GetString("SLACK_SIGNING_SECRET"),
	}, nil
}
