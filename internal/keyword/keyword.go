package keyword

import (
	"github.com/securisec/go-keywords"
	keywords_lang "github.com/securisec/go-keywords/languages"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
)

type KeyWordExtractor interface {
	Extract(text string, language string) ([]string, error)
}
type GoKeyWordExtractor struct {
	DefaultLanguage string
}

var languages = map[string][]string{
	"en": keywords_lang.English,
}

func (ke *GoKeyWordExtractor) Shutdown() {}

func (ke *GoKeyWordExtractor) Extract(text string, language string) ([]string, error) {
	if language == "" {
		language = ke.DefaultLanguage
	}
	return keywords.Extract(text, keywords.ExtractOptions{
		Language: languages[language],
	})
}

func NewKeyWordExtarction(cfg *config.Config) (*GoKeyWordExtractor, error) {
	return &GoKeyWordExtractor{
		DefaultLanguage: cfg.KeywordExtractDefaultLang,
	}, nil
}
