package server

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/classifier"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/config"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/configstore"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/db"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/gitlab"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers"
	v1_handlers "gitlab.com/suleim144/gitlab-al-truist-bot/internal/handlers/v1"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/keyword"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/logger"
	middleware "gitlab.com/suleim144/gitlab-al-truist-bot/internal/middleware"
	"gitlab.com/suleim144/gitlab-al-truist-bot/internal/slack"
)

func NewServer(cfg *config.Config, log logger.Logger, qd classifier.QuestionDetector, keyEx keyword.KeyWordExtractor) (server *http.Server, err error) {
	db, err := db.NewMongodb(cfg, log)
	if err != nil {
		return
	}
	configStore, err := configstore.NewConfigStore(db)
	if err != nil {
		return
	}
	var router http.Handler
	routeDependencies := handlers.RouteDependencies{
		Log:                  log,
		Cfg:                  cfg,
		ConfigStore:          configStore,
		Qd:                   qd,
		KeyEx:                keyEx,
		SlackComposerFactory: slack.RealSlackFactory{},
		GitlabClientFactory:  gitlab.RealGitLabClientFactory{},
	}
	router, err = newRouter(routeDependencies)
	if err != nil {
		return
	}

	log.Debug("creating server...")
	server = &http.Server{
		Addr:    "0.0.0.0:" + cfg.Port,
		Handler: router,
	}
	return
}

func newRouter(routeDependencies handlers.RouteDependencies) (http.Handler, error) {
	if !routeDependencies.Cfg.DebugMode {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.New()
	if routeDependencies.Cfg.IgnoreCORS {
		router.Use(middleware.IgnoreCORS())
	}
	router.Use(gin.Recovery())

	// TODO: Implement metric handler
	router.GET("/health", func(c *gin.Context) { c.JSON(200, gin.H{"status": "UP"}) })
	router.GET("/", func(c *gin.Context) { c.AbortWithStatus(http.StatusOK) })

	v1Routes := router.Group("/api/al/v1")
	v1Routes.Use(middleware.SetRequestContext(routeDependencies.Cfg.RequestTimeout))
	v1Routes.Use(middleware.RequestDumper(routeDependencies.Log))
	v1Routes.POST("/event", v1_handlers.EventHandler(routeDependencies))
	v1Routes.POST("/slack-oauth/token", v1_handlers.SlackTokenExchangeHandler(routeDependencies))
	v1Routes.POST("/slash-command/configure-channel", v1_handlers.ConfigureChannel(routeDependencies))
	v1Routes.POST("/interactive-event", v1_handlers.Dummy(routeDependencies))
	return router, nil
}
