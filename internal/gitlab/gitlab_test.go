package gitlab

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func TestUnPackIssues(t *testing.T) {
	gitLabIssues := []*gitlab.Issue{
		{
			Title:  "Title",
			WebURL: "WebURL",
		},
		{
			Title:  "Title2",
			WebURL: "WebURL2",
		},
	}
	expectedUnpackedIssues := []Issue{
		{
			Title: "Title",
			URL:   "WebURL",
		},
		{
			Title: "Title2",
			URL:   "WebURL2",
		},
	}
	assert.Equal(t, expectedUnpackedIssues, unPackIssues(gitLabIssues))
}
