package gitlab

import (
	"github.com/xanzy/go-gitlab"
)

type GitLabClient interface {
	GitLabIssueManagement
}

type GitLabIssueManagement interface {
	GetIssuesWithKeyWords(projectID, keywords string) ([]Issue, error)
}

type GitLabIssueController struct {
	Client *gitlab.Client
}
type GitLabClientFactory interface {
	NewClient(accessToken string) (GitLabClient, error)
}

type RealGitLabClientFactory struct{}

func (gcf RealGitLabClientFactory) NewClient(accessToken string) (GitLabClient, error) {
	return NewClientWithAccessToken(accessToken)
}

func NewClientWithAccessToken(accessToken string) (GitLabClient, error) {
	gitlabClient, err := gitlab.NewClient(accessToken)
	if err != nil {
		return nil, err
	}
	return &GitLabIssueController{
		Client: gitlabClient,
	}, nil
}

func (gic *GitLabIssueController) GetIssuesWithKeyWords(projectID, keywords string) ([]Issue, error) {
	issues, _, err := gic.Client.Issues.ListProjectIssues(projectID, &gitlab.ListProjectIssuesOptions{Search: &keywords})
	if err != nil {
		return nil, err
	}
	return unPackIssues(issues), nil
}

func unPackIssues(issues []*gitlab.Issue) []Issue {
	var unPackedissues []Issue
	for _, issue := range issues {
		unPackedissues = append(unPackedissues, Issue{
			Title: issue.Title,
			URL:   issue.WebURL,
		})
	}
	return unPackedissues

}

type Issue struct {
	Title string
	URL   string
}
